package com.luxoft.academy.oop.model;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.luxoft.academy.oop.GroupRegistry;

public class Teacher {

	private final String name;
	private final String surname;
	private Subject subject;

	public Teacher(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public boolean matches(String name, String surname) {
		return false;
	}
	
	public Lesson selectLesson(Date date, String topic ){
		List<Lesson> lessons = subject.getLessons();
		for(Lesson lesson: lessons){
			if( lesson.getDate().equals(date) && 
				lesson.getTopic().equals(topic)) {
				return lesson;
			}
		}
		return null;
	}
	
	public void markAttendance(GroupRegistry groupRegistry, Date date, String topic, String studentName,String studentSurname){
		Lesson lesson = selectLesson(date, topic);
		Student student = selectStudent(groupRegistry, studentName, studentSurname);
		//#TODO
	}
	
	public Student selectStudent(GroupRegistry groupRegistry, String studentName,String studentSurname){
		
	}

	// What if we wanted to setup relation between teacher and subject objects
	// while assigning a teacher to a subject?
	public void registerSubject(Subject subject) {
		this.subject = subject;
		subject.assignTeacher(this);
	}

	// How to ensure no one will modify returned collection of scheduled entries?
	public Collection<ScheduleEntry> getSchedule() {
		return subject.getScheduleEntries();
	}
}