package com.luxoft.academy.oop.model;

public class Grade {

	private final boolean finalGrade;
	private final GradeType type;
	private final Student student;

	public Grade(boolean finalGrade, GradeType type, Student student) {
		this.finalGrade = finalGrade;
		this.type = type;
		this.student = student;
	}

	public boolean isFinal() {
		return finalGrade;
	}

	public GradeType getType() {
		return type;
	}

	public Student getStudent() {
		return student;
	}
}