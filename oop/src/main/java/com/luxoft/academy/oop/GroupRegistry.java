package com.luxoft.academy.oop;

import com.luxoft.academy.oop.model.Group;
import com.luxoft.academy.oop.model.Student;
import com.luxoft.academy.oop.model.Subject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class GroupRegistry {

	private List<Group> groups;

	public void addGroup(Group group) {
		groups.add(group);
	}
	
	public void Group(String name, String profile) {
		Group group = new Group( name, profile);
		addGroup(group);
	}
	

	public List<Group> lookupGroup(String name, String profile) {
		List<Group> matchedGroups = new ArrayList<Group>();
		for(Group group: groups) {
			boolean matched = group.matches(name, profile);
			if(matched){
				matchedGroups.add(group);
			}
		}
		return matchedGroups;
	}
	
	public List<Student> displayStudents(String name, String profile){
		List<Student> result = new  ArrayList<Student>();
		List<Group> groups = lookupGroup(name, profile);
		for(Group group: groups ){
			List<Student> students = group.getStudents();
			for(Student student: students) {
					result.add(student);
			}
		}
		return result;
	}
	
	public Group getGroup(String studentName, String studentSurname){
		List<Group> result = new ArrayList<Group>();
		for(Group group: groups) {
			for (Student student: group.getStudents()) {
				if (student.getName().equals(studentName) && student.getSurname().equals(studentSurname)) {
					return group;
									}
					
			}
		}
		return null;
	}
	
	public Collection<Subject> displaySubjects(String name, String profile){
		Collection<Subject> result = new HashSet<Subject>();
		List<Group> groups = lookupGroup(name, profile);
		for(Group group: groups ){
			Collection<Subject> preResult = group.getSubjects();
			result.addAll(preResult);
		}
		return result;
	}
	
	public void registerStudent(String name, String profile, String studentName, String studentSurname){
		List<Group> groups = lookupGroup(name, profile);
		Student student = new Student(studentName,studentSurname);
		for(Group group: groups ){
			group.registerStudent(student);
		}
	}
	
	
	
}