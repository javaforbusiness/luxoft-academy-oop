package com.luxoft.academy.oop.model;

import java.util.List;

import static java.util.Collections.unmodifiableList;

public class Subject {

	private final String name;
	private List<Grade> grades;
	private List<ScheduleEntry> scheduleEntries;
	private List<Lesson> lessons;
	private Teacher teacher;

	public Subject(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addGrade(Grade grade) {
		grades.add(grade);
	}

	public List<Grade> getGrades() {
		return unmodifiableList(grades);
	}

	public void registerScheduleEntry(ScheduleEntry scheduleEntry) {
		scheduleEntries.add(scheduleEntry);
	}

	public List<ScheduleEntry> getScheduleEntries() {
		return unmodifiableList(scheduleEntries);
	}

	public void addLesson(Lesson lesson) {
		lessons.add(lesson);
	}

	public List<Lesson> getLessons() {
		return unmodifiableList(lessons);
	}

	public void assignTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
}