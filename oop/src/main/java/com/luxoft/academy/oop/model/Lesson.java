package com.luxoft.academy.oop.model;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

public class Lesson {

	private final Date date;
	private final String topic;
	private Collection<Student> students = new HashSet<Student>();

	public Lesson(Date date, String topic) {
		this.date = date;
		this.topic = topic;
	}

	public void markAttendance(Student student) {
		students.add(student);
	}

	public Date getDate() {
		return date;
	}

	public String getTopic() {
		return topic;
	}
}