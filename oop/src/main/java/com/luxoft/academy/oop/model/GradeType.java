package com.luxoft.academy.oop.model;

public enum GradeType {

	POOR,
	INSUFFICIENT,
	SATISFACTORY,
	GOOD,
	VERY_GOOD,
	EXCELLENT

}