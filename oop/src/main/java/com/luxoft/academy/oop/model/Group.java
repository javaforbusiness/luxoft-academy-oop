package com.luxoft.academy.oop.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class Group {

	private final String name;
	private final String profile;
	private List<Student> students = new ArrayList<Student>();
	private Collection<Subject> subjects = new HashSet<Subject>();
	
	public Group(String name, String profile) {
		this.name = name == null ? "" : name;
		this.profile = profile == null ? "" : profile;
	}

	public String getName() {
		return name;
	}

	public String getProfile() {
		return profile;
	}

	public Collection<ScheduleEntry> getSchedule() {
		return null;
	}

	public void registerStudent(Student student) {
		students.add(student);
	}

	public List<Student> getStudents() {
		return students;
	}

	public void registerSubject(Subject subject) {
		subjects.add(subject);
	}

	public Collection<Subject> getSubjects() {
		return subjects;
	}

	public boolean matches(String name, String profile) {
		return this.name.equals(name) && this.profile.equals(profile);
	}
	
	public Student selectStudent(String name, String surname){
		for(Student student: students){
			if (student.getName().equals(name) && 
					student.getSurname().equals(surname))
				return student;
		}
	return null;
	}
}