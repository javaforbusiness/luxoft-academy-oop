package com.luxoft.academy.oop.model;

import java.util.Collection;

public class Student {

	private final String name;
	private final String surname;

	public Student(String name, String surname) {
		this.name = name == null ? "": name;
		this.surname = surname == null ? "": surname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public Collection<ScheduleEntry> getSchedule() {
		return null;
	}
	
	public boolean equals(Student paramStudent){
		return (getName().equals(paramStudent.getName()) && getSurname().equals(paramStudent.getSurname()));
	}
}