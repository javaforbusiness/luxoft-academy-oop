package com.luxoft.academy.oop.model;

import java.util.Date;

public class ScheduleEntry {

	private final Weekday dayOfWeek;
	private final Date hour;
	private final int timeSpan;

	public ScheduleEntry(Weekday dayOfWeek, Date hour, int timeSpan) {
		this.dayOfWeek = dayOfWeek;
		this.hour = hour;
		this.timeSpan = timeSpan;
	}

	public Weekday getDayOfWeek() {
		return dayOfWeek;
	}

	public Date getHour() {
		return hour;
	}

	public int getTimeSpan() {
		return timeSpan;
	}
}